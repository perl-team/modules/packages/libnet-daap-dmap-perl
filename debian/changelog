libnet-daap-dmap-perl (1.27-2) UNRELEASED; urgency=medium

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jose Luis Rivas from Uploaders. Thanks for your work!

  [ Damyan Ivanov ]
  * change Priority from 'extra' to 'optional'

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- gregor herrmann <gregoa@debian.org>  Thu, 05 Jul 2012 12:44:35 -0600

libnet-daap-dmap-perl (1.27-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 08 Jan 2021 15:49:12 +0100

libnet-daap-dmap-perl (1.27-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ gregor herrmann ]
  * Email change: gregor herrmann -> gregoa@debian.org

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * New upstream release.
  * Switch to "3.0 (quilt)" source format.
  * Drop cdbs and quilt, use debhelper 8 and tiny debian/rules.
  * Remove patch, fixed upstream in this release.
  * Update debian/copyright.
  * Bump Standards-Version to 3.9.2 (no further changes).
  * Update long description.

 -- gregor herrmann <gregoa@debian.org>  Fri, 20 Jan 2012 17:07:34 +0100

libnet-daap-dmap-perl (1.26-2) unstable; urgency=low

  [ Stephen Gran ]
  * Lowered debhelper compatibility level to 5 (Be kind to backporters day)

  [ Damyan Ivanov ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza).

  [ gregor herrmann ]
  * debian/rules: delete /usr/lib/perl5 only if it exists.
  * Add patch pack_unpack_bytes.patch; the behaviour of pack() and unpack()
    regarding UTF-8 characters has changed in Perl 5.10, by adding "use
    bytes" we get the old behaviour (closes: #467277). Add quilt framework.
  * debian/control: Changed: Maintainer set to Debian Perl Group <pkg-
    perl-maintainers@lists.alioth.debian.org> (was: Jose Luis Rivas
    <ghostbar38@gmail.com>); Jose Luis Rivas <ghostbar38@gmail.com>
    moved to Uploaders. Add /me to Uploaders.
  * debian/watch: use dist-based URL.
  * Set Standards-Version to 3.7.3 (no changes).
  * Set debhelper compatibility level to 6.
  * Remove unnecessary debian/dirs.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Mon, 25 Feb 2008 19:07:53 +0100

libnet-daap-dmap-perl (1.26-1) unstable; urgency=low

  * Initial release (Closes: #420383)

 -- Jose Luis Rivas <ghostbar38@gmail.com>  Sat, 21 Apr 2007 18:32:13 -0400
